#! /bin/bash
# :set fileformat=unix     解决/bin/bash^M:bad interpreter:No such file or directory报错


current_path=`pwd`

log_path="${current_path}/LOGS"
pid_path="${current_path}/pid"
gunicorn_conf_path="gunicorn.conf.py"

BACK_PID="${pid_path}/django.pid"
WORKER_PID="${pid_path}/workers.pid"
BEAT_PID="${pid_path}/beat.pid"

PROJECT="Alpha"

# command----------
PYTHON="python"
CELERY="celery"
GUNICORN="gunicorn"
# -----------------

WORKER_LOG="${log_path}/celery_worker.txt"
BEAT_LOG="${log_path}/celery_beat.txt"

CELERY_LOG_LEVEL="INFO"

# 设置环境变量为PRO
function set_env() {
    export ENV="PRO"
}

# 检查是否存在pid文件夹
function check_PID() {
  if [ ! -d "${pid_path}" ]; then
  mkdir ${pid_path}
  fi
}


# django迁移数据库
function migrate() {
  ${PYTHON} manage.py makemigrations
  ${PYTHON} manage.py migrate
}

# django同步权限
function permission() {
  ${PYTHON} manage.py sync_permission
}


# 启动后台服务
function start_service() {
  check_PID
  if [ -f ${BACK_PID} ]
  then
    echo "django已经在运行状态..."
  else
    nohup ${GUNICORN} -c ${gunicorn_conf_path} ${PROJECT}.wsgi > /dev/null 2>&1 & echo $! > ${BACK_PID}
    echo "django启动成功"
  fi
}
# 结束后台服务
function stop_service() {
  check_PID
  if [ ! -f ${BACK_PID} ]
  then
    echo "未发现${BACK_PID},若程序未结束,请手动结束"
  else
    pid=`cat ${BACK_PID}`
    kill -s 15 ${pid}
    rm -rf ${BACK_PID}
    echo "pid:${pid}终止成功,django-service已终止"
  fi
}




# 启动 celery worker
function start_worker() {
  check_PID
  if [ -f ${WORKER_PID} ]
  then
    echo "celery worker已经在运行..."
  else
    nohup ${CELERY} -A ${PROJECT} worker -l ${CELERY_LOG_LEVEL} > ${WORKER_LOG} 2>&1 & echo $! > ${WORKER_PID}
    echo "celery worker启动成功"
  fi
}
# 终止 celery worker
function stop_worker() {
  check_PID
  if [ ! -f ${WORKER_PID} ]
  then
    echo "未发现${WORKER_PID},若程序未结束,请手动结束"
  else
    pid=`cat ${WORKER_PID}`
    kill -s 15 ${pid}
    rm -rf ${WORKER_PID}
    echo "pid:${pid}终止成功,celery worker服务已结束"
  fi
}



# 启动 celery beat
function start_beat() {
  check_PID
  if [ -f ${BEAT_PID} ]
  then
    echo "celery beat已经在运行..."
  else
    nohup ${CELERY} -A ${PROJECT} beat  -l ${CELERY_LOG_LEVEL} > ${BEAT_LOG} 2>&1 & echo $! > ${BEAT_PID}
    echo "celery beat启动成功"
  fi
}
# 终止 celery beat
function stop_beat() {
  check_PID
  if [ ! -f ${BEAT_PID} ]
  then
    echo "未发现${BEAT_PID},若程序未结束,请手动结束"
  else
    pid=`cat ${BEAT_PID}`
    kill -s 15 ${pid}
    rm -rf ${BEAT_PID}
    echo "pid:${pid}终止成功,celery beat服务已结束"
  fi
}



case $1 in

    start)
      set_env
      start_service
      start_worker
    ;;
    stop)
      stop_service
      stop_worker
    ;;

    service-start)
      set_env
      start_service
    ;;
    beat-start)
      set_env
      start_beat
    ;;
    worker-start)
      set_env
      start_worker
    ;;

   service-stop)
      stop_service
    ;;
    beat-stop)
      stop_beat
    ;;
    worker-stop)
      stop_worker
    ;;


    permission)
      permission
    ;;
    migrate)
      migrate
    ;;



    *)
      echo "Usage:
      ./deploy.sh [command]
      command**********************

      start (启动项目)
      stop (终止项目)

      service-start (启动 django)
      beat-start (启动 celery beat)
      worker-start (启动 celery worker)

      service-stop (启动 django)
      beat-stop (启动 celery beat)
      worker-stop (启动 celery worker)

      permission (同步权限)
      migrate (迁移数据库)

      ****************************"
    ;;
esac
