from auth_app.permissions import permissions as auth_permissions
from config_app.permissions import permissions as config_permissions
from celery_app.permissions import permissions as celery_permissions
from interface_test_app.permissions import permissions as interface_permission

Permissions = []

Permissions.extend(auth_permissions)
Permissions.extend(config_permissions)
Permissions.extend(celery_permissions)
Permissions.extend(interface_permission)
