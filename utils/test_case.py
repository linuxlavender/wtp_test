def test_result(req_obj):
    result = (
        r_time,
        r_text,
        r_headers
    ) = (
        req_obj.elapsed.total_seconds(),
        req_obj.text,
        req_obj.headers
    )
    [print(item) for item in result]
    return result
