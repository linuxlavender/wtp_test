from django.db import models
from auth_app.models import UserModel


# Create your models here.
class BaseModel(models.Model):
    id = models.AutoField(primary_key=True)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    create_by = models.ForeignKey(to=UserModel, related_name='+', null=True, on_delete=models.SET_NULL)
    update_by = models.ForeignKey(to=UserModel, related_name='+', null=True, on_delete=models.SET_NULL)
    is_valid = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Chioce:
    '''
    字段是数字，不同数字代表不同含义
    '''
    dict = {}

    def output(self, value):
        return self.dict[value] if value in self.dict.keys() else None
