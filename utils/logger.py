import loguru
from copy import deepcopy
from Alpha.settings import BASE_DIR


def _logger(path, level):
    loguru.logger.remove()
    log = deepcopy(loguru.logger)
    log.add(path, level=level)
    return log


def logger():
    return _logger(str(BASE_DIR) + '/LOGS/service.log', 'INFO')
