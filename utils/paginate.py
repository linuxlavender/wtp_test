def paginate(query_set, page, page_size):
    '''
    :param query_set:
    :param page:
    :param page_size:
    :return: new query_set
    '''
    new = query_set[(page - 1) * page_size:page_size * page]
    return new
