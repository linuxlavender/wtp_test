from django.test import TestCase
from utils.test_case import test_result
import requests, json


# Create your tests here.

class Test:
    def __init__(self):
        self.host = 'http://127.0.0.1:8000/'
        self.headers = {
            'cookie': 'SID=ba24eaa14d22335835636ed6c616a3d8'
        }


class SchedulerTest(Test):

    def create_interval(self):
        r = requests.request(**{
            'method': 'POST',
            'url': self.host + 'crontab/create_interval/',
            'json': {
                'every': 60,
                'period': 'seconds'
            },
            'headers': self.headers
        })
        test_result(r)

    def select_interval(self):
        r = requests.request(**{
            'method': 'GET',
            'url': self.host + 'crontab/get_interval/',
            'headers': self.headers
        })
        test_result(r)

    def select_interval_type(self):
        r = requests.request(**{
            'method': 'GET',
            'url': self.host + 'crontab/get_interval_type/',
            'headers': self.headers
        })
        test_result(r)


class AsyncTaskTest(Test):
    def result(self):
        r = requests.request(**{
            'method': 'GET',
            'url': self.host + 'celery/task/get_result/',
            'params': {
                'task_id': '9d88b4af-276c-476b-8f60-b162f3f6b4c3'
            },
            'headers': self.headers
        })
        test_result(r)


class TimingTaskTest(Test):

    def get_tasks(self):
        r = requests.request(**{
            'method': 'GET',
            'url': self.host + 'celery/timing/get_tasks/',
            'headers': self.headers
        })
        test_result(r)

    def create(self):
        r = requests.request(**{
            'method': 'POST',
            'url': self.host + 'celery/timing/create/',
            'headers': self.headers,
            'json': {
                'name': '定时任务1',
                'task': 'utils.timing.test_task',
                'interval': '100',
                # 'crontab': '',
                # 'solar': '',
                # 'clocked': '',
                'kwargs': json.dumps({'a': 'zxc', 'b': 'qwer'}),
                'queue': '',
                'expire_seconds': '',
                'one_off': True,
                'enabled': True,
                'description': '水电费金黄色的快乐番薯的李开复',
            }
        })
        test_result(r)

    def update(self):
        r = requests.request(**{
            'method': 'POST',
            'url': self.host + 'celery/timing/update/',
            'headers': self.headers,
            'json': {
                'id': 1,
                'name': '定时任务1',
                'task': 'tasks.timing_tasks.test_task',
                'interval': '1',
                'crontab': '',
                'solar': '',
                'clocked': '',
                'kwargs': json.dumps({'a': 'zxc', 'b': 'qwer'}),
                'queue': '',
                'expire_seconds': '',
                'one_off': False,
                'enabled': False,
                'description': '水电费金黄色的快乐番薯的李开复',
            }
        })
        test_result(r)

    def select(self):
        r = requests.request(**{
            'method': 'GET',
            'url': self.host + 'celery/timing/select/',
            'headers': self.headers,
            'params': {
                'page': 1,
                'page_size': 10,
            }
        })
        test_result(r)


if __name__ == '__main__':
    test = AsyncTaskTest()
    test.result()
