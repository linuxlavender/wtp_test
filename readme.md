# WTP 测试平台

## 演示地址 [http://www.icpy.com.cn:8080](http://www.icpy.com.cn:8080)
![image](图片/wtp.png)
#### 帐号
```
test-user test-user1 test-user2 test-user3 test-user4
test-user5 test-user6 test-user7 test-user8 test-user9
test-user10
密码:12345678
```
#### 后端技术栈
* django
* celery
* requests
* PyJWT
* xlrd、xlwt
* gunicorn
* mysql
* redis

#### 前端技术栈(稍后开源)
项目前端使用开源框架构建 [vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)
* vue
* vuex
* axios
* element-ui


## 部署
#### 服务器部署
1、 配置环境、安装依赖
```
pip install -r requirements.txt
```
2、 创建配置文件
```
根据运行环境的不同，自动选择配置文件
conf.dev.json  # 开发环境
conf.prod.json  # 生产环境
```
* 配置文件内容
```json
{
  "storage": {
    "mysql": {
      "host": "xxx",
      "port": 3306,
      "user": "xxx",
      "password": "xxx",
      "database": "xxx"
    }
  },
  "celery": {
    "task_serializer": "json",
    "accept_content": [
      "json"
    ],
    "result_serializer": "json",
    "broker_url": "redis://:password@host:port/database",
    "result_backend": "django-db",
    "worker_concurrency": 2,
    "timezone": "Asia/Shanghai",
    "worker_max_tasks_per_child": 100
  },
  "application": {
    "user_token_name": "User-Token",
    "record_log": true,
    "allowed_domain": [
      "http://127.0.0.1",
      "其他允许跨域的域名..."
    ]
  }
}
```
3、 数据库迁移
```
python manage.py makemigrations
python manage.py migrate
```
4、同步权限
* 自定义命令文件 auth/management/commands/sync_permission.py
```
python manage.py sync_permission
```
5、创建帐号
* 自定义命令文件 auth/management/commands/create_user.py
```
python manage.py create_user user_name user_password 1     # 创建超级管理员
python manage.py create_user user_name user_password 0     # 创建普通用户
```
6、运行项目
* wsgi
```
python manage.py runserver host:port  --noreload
```
* gunicorn
```
gunicorn -c gunicorn.conf.py Alpha.wsgi
```
7、运行celery worker(独立于后端服务)
```
celery -A Alpha worker -l INFO
```
#### docker部署
1、打包image文件
```
docker build -t wtp_backend:v1 .
```
2、运行镜像
```
docker run -it -d --name wtp_back -p port_inside:port_outside wtp_backend:v1
```
3、进入镜像
```
docker exec -it wtp_back /bin/bash
```
4、执行deploy脚本
```
./deploy migrate
./deploy permission
./deploy start
```


