from django.apps import AppConfig


class InterfaceTestAppConfig(AppConfig):
    name = 'interface_test_app'
