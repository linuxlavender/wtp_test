from django.urls import path
from .views.case import CaseView
from .views.executor import ExecuteView
from .views.publish import PubParamView
from .views.case_set import CaseSetView
from .views.report import ReportView

# (url,view,method,user_verify)

urls = (
    ('interface/case/info/', CaseView().info, 'POST', True),
    ('interface/case/create/', CaseView().create, 'POST', True),
    ('interface/case/update/', CaseView().update, 'POST', True),
    ('interface/case/delete/', CaseView().delete, 'POST', True),
    ('interface/case/list/', CaseView().list, 'POST', True),
    ('interface/case/detail/', CaseView().case_detail, 'POST', True),
    ('interface/case/copy/', CaseView().copy, 'POST', True),

    ('interface/case_set/create/', CaseSetView().create, 'POST', True),
    ('interface/case_set/update/', CaseSetView().update, 'POST', True),
    ('interface/case_set/delete/', CaseSetView().delete, 'POST', True),
    ('interface/case_set/list/', CaseSetView().list, 'POST', True),
    ('interface/case_set/register/update/', CaseSetView().register, 'POST', True),
    ('interface/case_set/register/list/', CaseSetView().cases_with_current_set, 'POST', True),

    ('interface/pub_param/insert/', PubParamView().insert, 'POST', True),
    ('interface/pub_param/delete/', PubParamView().delete, 'POST', True),
    ('interface/pub_param/update/', PubParamView().update, 'POST', True),
    ('interface/pub_param/list/', PubParamView().list, 'POST', True),

    ('interface/execute/case/', ExecuteView().execute_case, 'POST', True),
    ('interface/execute/set/', ExecuteView().execute_CaseSet, 'POST', True),

    ('interface/report/list/', ReportView().list, 'POST', True),
    ('interface/report/delete/', ReportView().delete, 'POST', True),
    ('interface/report/details/', ReportView().details, 'POST', True),

)

urlpatterns = [path(i[0], i[1]) for i in urls]
