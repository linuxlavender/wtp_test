from django.test import TestCase
from utils.test_case import test_result
import requests


# Create your tests here.


class AuthTest:
    def __init__(self):
        self.host = 'http://127.0.0.1:8000/'
        self.headers = {
            'User-Token': ''
        }
        self._get_token()

    def _get_token(self):
        with open('../token', 'r', encoding='UTF-8') as f:
            token = f.read().replace('\n', '')

        self.headers['User-Token'] = token

    def login(self):
        r = requests.request(**{
            'method': 'POST',
            'url': self.host + 'auth/login/',
            'json': {
                'usr_name': 'admin',
                'usr_pwd': '123456',
            }
        })
        test_result(r)

    def logout(self):
        r = requests.request(**{
            'method': 'POST',
            'url': self.host + 'auth/logout/',
            'headers': self.headers
        })
        test_result(r)

    def change_pwd(self):
        r = requests.request(**{
            'method': 'POST',
            'url': self.host + 'auth/change_pwd/',
            'headers': self.headers,
            'json': {
                'new_pwd': '123456'
            }
        })
        test_result(r)


def test_create_role():
    r = requests.request(**{
        'method': 'POST',
        'url': host + 'role/create_roles/',
        'headers': headers,
        'json': {
            'role_name': 'role5',
            'role_desc': 'abc'
        }
    })
    test_result(r)


def test_get_role():
    r = requests.request(**{
        'method': 'GET',
        'url': host + 'role/get_roles/',
        'headers': headers
    })
    test_result(r)


def test_delete_role():
    r = requests.request(**{
        'method': 'POST',
        'url': host + 'role/delete_roles/',
        'headers': headers,
        'json': {
            'role_id': 5
        }
    })
    test_result(r)


def test_get_role_by_usr():
    r = requests.request(**{
        'method': 'GET',
        'url': host + 'role/get_role_by_user/',
        'headers': headers,
        'params': {
            'user_id': 12
        }
    })
    test_result(r)


def test_register_role_to_user():
    r = requests.request(**{
        'method': 'POST',
        'url': host + 'role/register_role_user/',
        'headers': headers,
        'json': {
            'user_id': 11,
            'role_id': 3
        }
    })
    test_result(r)


def test_dismiss_role_to_user():
    r = requests.request(**{
        'method': 'POST',
        'url': host + 'role/dismiss_role_user/',
        'headers': headers,
        'json': {
            'user_id': 11,
            'role_id': 2
        }
    })
    test_result(r)


def test_get_permission():
    r = requests.request(**{
        'method': 'GET',
        'url': host + 'permission/get_permissions/',
        'headers': headers,

    })
    test_result(r)


def test_register_permission_to_role():
    r = requests.request(**{
        'method': 'POST',
        'url': host + 'permission/register_permission_role/',
        'headers': headers,
        'json': {
            'role_id': 1,
            'permission_id': 8
        }

    })
    test_result(r)


def test_dismiss_permission_to_role():
    r = requests.request(**{
        'method': 'POST',
        'url': host + 'permission/dismiss_permission_role/',
        'headers': headers,
        'json': {
            'role_id': 1,
            'permission_id': 6
        }

    })
    test_result(r)


def test_get_log():
    r = requests.request(**{
        'method': 'GET',
        'url': host + 'log/get_log/',
        'headers': headers,
        'params': {
            'page': 1,
            'page_size': 10
        }

    })
    test_result(r)


if __name__ == '__main__':
    auth = AuthTest()
    auth.logout()
