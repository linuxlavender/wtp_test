from django.core.management.base import BaseCommand
from auth_app.models import PermissionModel
from Alpha.permissions import Permissions

class Command(BaseCommand):

    def sync_permission(self):
        for p in Permissions:
            if not PermissionModel.objects.filter(id=p['id']):
                PermissionModel.objects.create(**{
                    'id': p['id'],
                    'app': p['app'],
                    'name': p['name'],
                    'operate': p['operate']
                })

    def handle(self, *args, **options):
        self.sync_permission()
