from django import forms
from utils.field import ListField
from utils.form import BaseForm


class AuthForm:
    class Login(BaseForm):
        usr_name = forms.CharField()
        usr_pwd = forms.CharField()

    class ChangePassword(BaseForm):
        new_pwd = forms.CharField()


class UserForm:
    class UserList(BaseForm):
        page = forms.IntegerField(min_value=1)
        page_size = forms.IntegerField(min_value=1)

    class Update(BaseForm):
        id = forms.IntegerField()
        usr_mail = forms.EmailField(required=False, empty_value=None)
        usr_phone = forms.IntegerField(required=False)
        is_valid = forms.NullBooleanField(required=False)
        is_super = forms.NullBooleanField(required=False)

        def clean(self):
            result = {}
            data = self.cleaned_data
            for k, v in data.items():
                if v != None:
                    result.setdefault(k, v)
            return result


class RoleForm:
    class RoleCreate(BaseForm):
        role_name = forms.CharField()
        role_desc = forms.CharField()

    class RoleDelete(BaseForm):
        role_id = forms.IntegerField()

    class RoleList(BaseForm):
        page = forms.IntegerField(min_value=1)
        page_size = forms.IntegerField(min_value=1)

    class RoleGet(BaseForm):
        user_id = forms.IntegerField()

    class UpdateRoleUser(BaseForm):
        user_id = forms.IntegerField()
        role_ids = ListField()


class PermissionForm:
    class PermissionGet(BaseForm):
        role_id = forms.IntegerField()

    class UpdatePermissionRole(BaseForm):
        role_id = forms.IntegerField()
        permission_ids = ListField()


class LogForm:
    class LogList(BaseForm):
        page = forms.IntegerField(min_value=1)
        page_size = forms.IntegerField(min_value=1)
