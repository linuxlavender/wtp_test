# 继承python3.7镜像
FROM python:3.7

 # 作者
MAINTAINER WangXingYua

# 设置python环境变量
ENV PYTHONUNBUFFERED 1

# 设置默认pip conf文件，指定pip源
COPY pip.conf /root/.pip/pip.conf

# 创建项目地址
RUN mkdir -p /Alpha

# 指定工作目录
WORKDIR /Alpha

# 将当前目录添加至指定目录
ADD . /Alpha

# pip安装依赖
RUN pip install -r requirements.txt

# 给deploy.sh添加执行权限，deploy为部署脚本
RUN chmod +x ./deploy.sh

# windows下编辑的sh文件每行会有多于的\r字符,需要执行命令删除
RUN sed -i 's/\r//' ./deploy.sh
