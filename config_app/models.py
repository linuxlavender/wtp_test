from django.db import models
from auth_app.models import UserModel
from utils.model import BaseModel


# Create your models here.


class DatabaseModel(BaseModel):
    verbose_name = '数据库管理'
    host = models.CharField(max_length=20)
    port = models.IntegerField()
    user = models.CharField(max_length=50)
    password = models.CharField(max_length=100)
    database = models.CharField(max_length=50)
    desc = models.CharField(max_length=500)
    type = models.CharField(max_length=10)  # mysql,oracle

    class Meta:
        db_table = 'config_database'


class ProjectModel(BaseModel):
    verbose_name = '项目管理'
    proj_name = models.CharField(max_length=50)
    proj_desc = models.CharField(max_length=500)
    to_user = models.ManyToManyField(to=UserModel, related_name='to_user', through='ProjectUserModel')

    class Meta:
        db_table = 'config_project'


class ProjectUserModel(models.Model):
    proj = models.ForeignKey(to=ProjectModel, related_name='P_id', null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(to=UserModel, related_name='U_id', null=True, on_delete=models.SET_NULL)

    class Meta:
        db_table = 'config_proj_user'


class EnvironmentModel(BaseModel):
    verbose_name = '测试环境管理'
    name = models.CharField(max_length=50, unique=True)
    host = models.CharField(max_length=100)
    port = models.IntegerField(default=80)

    class Meta:
        db_table = 'conf_environment'
