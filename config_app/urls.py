from django.urls import path
from .views import DatabaseView, ProjectView, EnvView

# (url,view,method,user_verify)

urls = (
    ('config/database/insert/', DatabaseView().insert, 'POST', True),
    ('config/database/delete/', DatabaseView().delete, 'POST', True),
    ('config/database/update/', DatabaseView().update, 'POST', True),
    ('config/database/list/', DatabaseView().list, 'POST', True),
    ('config/database/connect/', DatabaseView().connect, 'POST', True),

    ('config/project/insert/', ProjectView().insert, 'POST', True),
    ('config/project/delete/', ProjectView().delete, 'POST', True),
    ('config/project/update/', ProjectView().update, 'POST', True),
    ('config/project/list/', ProjectView().list, 'POST', True),

    ('config/environment/list/', EnvView().list, 'POST', True),
    ('config/environment/insert/', EnvView().insert, 'POST', True),
    ('config/environment/update/', EnvView().update, 'POST', True),
    ('config/environment/delete/', EnvView().delete, 'POST', True),

)

urlpatterns = [path(i[0], i[1]) for i in urls]
